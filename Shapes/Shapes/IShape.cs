﻿using System;

namespace Shapes
{
    public interface IShape
    {
        double Square();
    }
}